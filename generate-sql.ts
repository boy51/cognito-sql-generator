import fs from "fs";
import rawJson from "./data/eu-central-1_JEnk8fPWJ.json";

type Ent = {
  Username: string;
  Attributes: { Name: string; Value: string }[];
  Enabled: boolean;
};

type ResolvedEnt = {
  givenName: string;
  familyName: string;
  picture?: string;
  id: string;
};

function getAttr(user: Ent): ResolvedEnt {
  const id = user.Username;
  const givenName = user.Attributes?.find(att => att.Name === "given_name")?.Value;
  const familyName = user.Attributes?.find(att => att.Name === "family_name")?.Value;
  const picture = user.Attributes?.find(att => att.Name === "picture")?.Value;

  if (!id || !givenName || !familyName) {
    throw new Error(
      `Could not update user with id ${id}: expected first and lastname but got ${givenName} ${familyName}`
    );
  }

  return {
    id,
    givenName: givenName.replace("'", "&#39;"),
    familyName: familyName.replace("'", "&#39;"),
    picture,
  };
}

function genQuery(ent: ResolvedEnt): string {
  let updateQuery =
    'update "user" set "givenName" = $1, "familyName" = $2, "picture" = $3 where id = $4';

  updateQuery = updateQuery.replace("$1", `'${ent.givenName}'`);
  updateQuery = updateQuery.replace("$2", `'${ent.familyName}'`);
  if (ent.picture) {
    updateQuery = updateQuery.replace("$3", `'${ent.picture}'`);
  } else {
    updateQuery = updateQuery.replace(', "picture" = $3', "");
  }

  updateQuery = updateQuery.replace("$4", `'${ent.id}'`);
  updateQuery = updateQuery += ";";

  return updateQuery;
}

let i = 0;
const filename = Date.now() + ".sql";
const filepath = `${__dirname}/data/${filename}`;
fs.writeFileSync(filepath, "");
for (const entity of rawJson as Ent[]) {
  try {
    const query = genQuery(getAttr(entity));
    fs.appendFileSync(filepath, query + "\n");

    i++;
  } catch (err) {
    console.error(err);
    continue;
  }
}

console.log("lines written: ", i);
console.log("Done.");
